<?php
namespace Bmi\Tests;

class ApiTest extends \PHPUnit\Framework\TestCase
{
	public function testApiRequest()
	{
		$client = new \GuzzleHttp\Client();
		$response = $client->request(
			'GET',
			'http://bmi.didiksetiawan.com',
			[
				'query' => [
					'weight' => 85,
					'height' => 185
				]
			]
		);

		$result = $response->getBody()->getContents();

		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . '/../Fixtures/api-result.json',
			$result
		);
	}
}
