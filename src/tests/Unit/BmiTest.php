<?php
namespace Bmi\Tests;

use \Bmi\Classes;

class BmiTest extends \PHPUnit\Framework\TestCase {
	/**
	 * @param $original
	 * @param $expected
	 *
	 * @dataProvider providerTestDescription
	 */
	public function testDescription($original, $expected)
	{
		$strategy = new \Bmi\Classes\MetricUnitStrategy();

		$Bmi = new \Bmi\Classes\Bmi($original[0], $original[1], $strategy);
		$data = $Bmi->calculate();

		$this->assertEquals($data['label'], $expected);
	}

	/**
	 * @return array
	 */
	public function providerTestDescription()
	{
		return [
			[
				[220, 85, 'metric'], 'underweight'
			],
			[
				[185, 85, 'metric'], 'normal'
			],
			[
				[170, 85, 'metric'], 'overweight'
			],
		];
	}
}
