# BMI Calculator
REST API example for calculating BMI using PHP

## Installation

- Native setup using php and composer:

``` bash
$ composer install --no-dev
```

- Or Using docker:
``` bash
$ docker run --rm -p 80:8080 dstw/bmi-php
```

## Params

Parameters via GET request:
 
- height (cm)
- weight (kg)

## Demo:

<b>Live demo:</b>

<b>API response</b> - calculate BMI for weight 70 kg, height 167 cm (metric):

URL: http://bmi.didiksetiawan.com/?weight=70&height=167

CURL example:
```
$ curl "http://bmi.didiksetiawan.com/?weight=70&height=167"

[{"bmi":25.1,"label":"overweight"}]
```

## References

http://www.whathealth.com/bmi/formula.html  
https://github.com/Bakharevich/REST-API-Example  
https://github.com/TrafeX/docker-php-nginx  
https://kubernetes.io/docs/concepts/services-networking/ingress  
https://grafana.com/docs/loki/latest/installation/helm/   
