FROM composer AS composer

# copying the source directory and install the dependencies with composer
COPY src/ /app

# run composer install to resolve dependencies
RUN composer install --no-dev

# continue stage build with the desired image and copy the source including the
# dependencies downloaded by composer
FROM trafex/php-nginx
RUN rm -rf /var/www/html/*
COPY --chown=nobody --from=composer /app /var/www/html
